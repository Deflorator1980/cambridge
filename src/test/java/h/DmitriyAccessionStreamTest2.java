package h;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

public class DmitriyAccessionStreamTest2 {
    public final static String SYNID = "#NOT#MATCHED#";
    public final static String SYNID_MASK = "";
    public final static String MASK = "0";


    private static String[] match_split(String s) {
        Matcher m = null;
        return (m = Pattern.compile("(\\D+)(\\d+)").matcher(s)).find() && 2 == m.groupCount() ? new String[]{m.group(1), m.group(2)}
                : new String[]{SYNID, s};
    }


    private static List<SimpleEntry<String, String>>
    entry_reduce(List<SimpleEntry<String, String>> a, List<SimpleEntry<String, String>> e) {
        if (!a.isEmpty() && 1 >= -Integer.parseInt(a.get(a.size() - 1).getValue(), 10) + Integer.parseInt(e.get(0).getValue(), 10))
            a.get(a.size() - 1).setValue(e.get(0).getValue());
        else
            a.addAll(e);
        return a;
    }


    Map<Entry<String, String>, List<String>> getRanges(Collection<String> accession_list) {
        return
                accession_list.stream()
                        .collect(Collectors.groupingBy(e -> new SimpleEntry<String, String>(match_split(e)[0], Stream.generate(() -> MASK).limit(e.length()).collect(Collectors.joining())),
                                Collectors.mapping(e -> match_split(e)[1],
                                        Collectors.toList())))
                        .entrySet()
                        .stream()
                        .map((e) -> SYNID == e.getKey().getKey() ? new SimpleEntry<Entry<String, String>, List<String>>(new SimpleEntry<String, String>(SYNID, SYNID_MASK),
                                e.getValue()
                                        .stream()
                                        .sorted()
                                        .collect(Collectors.toList()))

                                : new SimpleEntry<Entry<String, String>, List<String>>(e.getKey(),
                                e.getValue()
                                        .stream()
                                        .sorted((o1, o2) -> Integer.compare(Integer.parseInt(o1, 10), Integer.parseInt(o2, 10)))
                                        .map(i -> Arrays.asList(new SimpleEntry<String, String>(i, i)))
                                        .reduce(new ArrayList<>(), DmitriyAccessionStreamTest2::entry_reduce)
                                        .stream()
                                        .map(i -> i.getKey() == i.getValue() ? e.getKey().getKey() + i.getKey() : e.getKey().getKey() + i.getKey() + '-' + e.getKey().getKey() + i.getValue())
                                        .collect(Collectors.toList())))
                        .collect(Collectors.toMap(i -> i.getKey(), i -> i.getValue(),
                                (v1, v2) -> {
                                    v1.addAll(v2);
                                    return v1.stream().sorted().collect(Collectors.toList());
                                }));
    }


    List<String>
    getRangeList(Collection<String> accession_list) {
        return getRanges(accession_list).entrySet()
                .stream()
                .sorted((e1, e2) -> e1.getKey().toString().compareTo(e2.getKey().toString()))
                .map(e -> e.getValue())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }


    @Test
    public void testRanges() {
        List<String> list = Arrays.asList("00000", "0001", "ERR000111", "ERR000112", "ERR200000001", "ERR200000002",
                "ERR200000003", "DRR211001", "SRR211001", "ABCDEFG");

        Map<Entry<String, String>, List<String>> result = getRanges(list);
        System.out.println(result);

        //keys
        Assert.assertEquals(5, result.size());
        Assert.assertTrue(result.keySet().stream().filter(e -> e.getKey().equals(SYNID)).findFirst().isPresent());
        Assert.assertTrue(result.keySet().stream().filter(e -> e.getKey().equals("ERR") && Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(9).collect(Collectors.joining()).equals(e.getValue())).findFirst().isPresent());
        Assert.assertTrue(result.keySet().stream().filter(e -> e.getKey().equals("ERR") && Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(12).collect(Collectors.joining()).equals(e.getValue())).findFirst().isPresent());
        Assert.assertTrue(result.keySet().stream().filter(e -> e.getKey().equals("SRR")).findFirst().isPresent());
        Assert.assertTrue(result.keySet().stream().filter(e -> e.getKey().equals("DRR")).findFirst().isPresent());

        //ranges and orders
        Assert.assertEquals(1, result.get(new SimpleEntry<String, String>("ERR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(9).collect(Collectors.joining()))).size());
        Assert.assertTrue(result.get(new SimpleEntry<String, String>("ERR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(9).collect(Collectors.joining()))).get(0).matches("ERR000111.{1,}ERR000112"));
        Assert.assertEquals(1, result.get(new SimpleEntry<String, String>("ERR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(12).collect(Collectors.joining()))).size());
        Assert.assertTrue(result.get(new SimpleEntry<String, String>("ERR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(12).collect(Collectors.joining()))).get(0).matches("ERR200000001.{1,}ERR200000003"));

        Assert.assertEquals(1, result.get(new SimpleEntry<String, String>("SRR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(9).collect(Collectors.joining()))).size());
        Assert.assertEquals(1, result.get(new SimpleEntry<String, String>("DRR", Stream.generate(() -> DmitriyAccessionStreamTest2.MASK).limit(9).collect(Collectors.joining()))).size());

        Assert.assertEquals(3, result.get(new SimpleEntry<String, String>(DmitriyAccessionStreamTest2.SYNID, DmitriyAccessionStreamTest2.SYNID_MASK)).size());

        Assert.assertTrue(result.get(new SimpleEntry<String, String>(DmitriyAccessionStreamTest2.SYNID, DmitriyAccessionStreamTest2.SYNID_MASK)).get(0).matches("00000"));
        Assert.assertTrue(result.get(new SimpleEntry<String, String>(DmitriyAccessionStreamTest2.SYNID, DmitriyAccessionStreamTest2.SYNID_MASK)).get(1).matches("0001"));
        Assert.assertTrue(result.get(new SimpleEntry<String, String>(DmitriyAccessionStreamTest2.SYNID, DmitriyAccessionStreamTest2.SYNID_MASK)).get(2).matches("ABCDEFG"));

        System.out.println(getRangeList(list));

        List<String> accessionList = Arrays.asList("A00000",
                "A00000",
                "A00001",
                "A0001",
                "A0003",
                "ERR000111",
                "ERR000112",
                "ERR000113",
                "ERR000115",
                "ERR000116",
                "ERR100114",
                "ERR200000001",
                "ERR200000002",
                "ERR200000003",
                "DRR2110012",
                "SRR211001",
                "ABCDEFG1");

        List<String> accessionRangeList = getRangeList(accessionList);

        System.out.println(accessionRangeList);

        Assert.assertEquals(Arrays.asList("A0001",
                "A0003",
                "A00000-A00001",
                "ABCDEFG1",
                "DRR2110012",
                "ERR000111-ERR000113",
                "ERR000115-ERR000116",
                "ERR100114",
                "ERR200000001-ERR200000003",
                "SRR211001"), accessionRangeList);
    }
}
