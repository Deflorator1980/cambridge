package h;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Dimitriy {
    public final static String SYNID = "#NOT#MATCHED#";
    public final static String SYNID_MASK = "";
    public final static String MASK = "0";

    private static String[] match_split(String s) {
        Matcher m = null;
        return (m = Pattern.compile("(\\D+)(\\d+)").matcher(s)).find() && 2 == m.groupCount() ? new String[]{m.group(1), m.group(2)}
                : new String[]{SYNID, s};
    }

    private static List<AbstractMap.SimpleEntry<String, String>> entry_reduce(List<AbstractMap.SimpleEntry<String, String>> a, List<AbstractMap.SimpleEntry<String, String>> e) {
        if (!a.isEmpty() && 1 >= -Integer.parseInt(a.get(a.size() - 1).getValue(), 10) + Integer.parseInt(e.get(0).getValue(), 10))
            a.get(a.size() - 1).setValue(e.get(0).getValue());
        else
            a.addAll(e);
        return a;
    }

    Map<Map.Entry<String, String>, List<String>> getRanges(Collection<String> accession_list) {
        return
                accession_list.stream()
                        .collect(Collectors.groupingBy(e -> new AbstractMap.SimpleEntry<String, String>(match_split(e)[0], Stream.generate(() -> MASK).limit(e.length()).collect(Collectors.joining())),
                                Collectors.mapping(e -> match_split(e)[1],
                                        Collectors.toList())))
                        .entrySet()
                        .stream()
                        .map((e) -> SYNID == e.getKey().getKey() ? new AbstractMap.SimpleEntry<Map.Entry<String, String>, List<String>>(new AbstractMap.SimpleEntry<String, String>(SYNID, SYNID_MASK),
                                e.getValue()
                                        .stream()
                                        .sorted()
                                        .collect(Collectors.toList()))

                                : new AbstractMap.SimpleEntry<Map.Entry<String, String>, List<String>>(e.getKey(),
                                e.getValue()
                                        .stream()
                                        .sorted((o1, o2) -> Integer.compare(Integer.parseInt(o1, 10), Integer.parseInt(o2, 10)))
                                        .map(i -> Arrays.asList(new AbstractMap.SimpleEntry<String, String>(i, i)))
                                        .reduce(new ArrayList<>(), Dimitriy::entry_reduce)
                                        .stream()
                                        .map(i -> i.getKey() == i.getValue() ? e.getKey().getKey() + i.getKey() : e.getKey().getKey() + i.getKey() + '-' + e.getKey().getKey() + i.getValue())
                                        .collect(Collectors.toList())))
                        .collect(Collectors.toMap(i -> i.getKey(), i -> i.getValue(),
                                (v1, v2) -> {
                                    v1.addAll(v2);
                                    return v1.stream().sorted().collect(Collectors.toList());
                                }));
    }


    List<String>getRangeList(Collection<String> accession_list) {
        return getRanges(accession_list).entrySet()
                .stream()
                .sorted((e1, e2) -> e1.getKey().toString().compareTo(e2.getKey().toString()))
                .map(e -> e.getValue())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
//        List<String> list = Arrays.asList("00000", "0001", "ERR000111", "ERR000112", "ERR200000001", "ERR200000002",
//                "ERR200000003", "DRR211001", "SRR211001", "ABCDEFG");
//        List<String> accessionList = Arrays.asList("A00000","A00000","A00001","A0001","A0003","ERR000111","ERR000112",
//                "ERR000113","ERR000115","ERR000116","ERR100114","ERR200000001","ERR200000002","ERR200000003","DRR2110012","SRR211001","ABCDEFG1");
        List<String> accessionList = Arrays.asList("A00000", "A0001", "ERR000111", "ERR000112", "ERR000113", "ERR000114", "ERR000115",
                "ERR000116", "ERR100114", "ERR200000001", "ERR200000002", "ERR200000003", "DRR2110012", "DRR2110013", "DRR2110014", "DRR21100144", "SRR211001", "ABCDEFG1");
        Dimitriy dimitriy = new Dimitriy();
        List<String> accessionRangeList = dimitriy.getRangeList(accessionList);
        System.out.println(accessionList);
        System.out.println(accessionRangeList);

    }
}
