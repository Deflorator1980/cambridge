//Rextester.Program.Main is the entry point for your code. Don't change it.
//Compiler version 4.0.30319.17929 for Microsoft (R) .NET Framework 4.5

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rextester {
    public class Program {
        public static void Main (string[] args) {
            var Arr = new string[] { "ERR200000002", "A00000", "A0001", "ERR000111", "ERR000113", "ERR000115", "ERR000116", "ERR100114", "ERR200000001", "ERR200000003", "DRR2110012", "SRR211001", "ABCDEFG1", "ERR000112", };

            var result =
                Arr
                .Select (x => new { Acc = getLetters (x), Num = getDigits (x) })
                .OrderBy (x => x.Acc)
                .ThenBy (x => x.Num)
                .GroupWhile ((x1, x2) => x1.Num.Length == x2.Num.Length && Int32.Parse (x2.Num) - Int32.Parse (x1.Num) == 1)
                .Select (g => {
                    var min = g.Min (c => c.Num);
                    var max = g.Max (c => c.Num);
                    var acc = g.First ().Acc;
                    return new { S = min == max? acc + min : acc + min + "-" + acc + max };
                });

            foreach (var k in result)
                Console.WriteLine (k.S);

        }
    }
}